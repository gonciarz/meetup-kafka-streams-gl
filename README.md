# Meetup

## Requirements

1. Docker compose
2. JDK 11+

## How to run

1. Start Kafka broker and Cassandra

    cd meet-infrastructure
    docker-compose up -d

2. For each service run:

    ./gradlew bootRun


